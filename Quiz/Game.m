//
//  Game.m
//  Quiz
//
//  Created by Daniel Ungerfält on 03/02/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import "Game.h"
#import "ViewController.h"
#import <Foundation/Foundation.h>

@interface Game()
@property (nonatomic) NSArray *rightAnswers;
@property (nonatomic) NSArray *Answer1;
@property (nonatomic) NSArray *Answer2;
@property (nonatomic) NSArray *Answer3;
@property (nonatomic) NSArray *Answer4;
@property (nonatomic) NSArray *questions;
@property (nonatomic) int Idtest;
@property(nonatomic) int questionId;
@end

@implementation Game

- (instancetype)init {
    self = [super init];
    if (self) {
        [self generateNewQuestion];
        
    }
    return self;
}

-(void)generateNewQuestion {
    self.Idtest = arc4random()% 10;
    
    self.questions = @[@"Who originally wrote the song All along the watchtower?",@"What band has the second best selling album of all time?",@"What is the Beatles first album called?",@"Foo Fighter's frontman and former drummer for the band Nirvana?",@"The Toxic Twins is a nickname given to?",@"The Stratocaster is a model of which guitar maker?",@"What record label did the Beatles establish?",@"Who was the youngest sibling in the Jackson 5?",@"What year did 2pac die?",@"What’s the name of the singer in the Swedish band ”Kent”?"];
    self.rightAnswers = @[@"Bob Dylan",@"ACDC",@"PleaJocke Bergse Please Me",@"Dave Grohl",@"Tyler and Perry",@"Fender",@"Apple",@"Randy",@"1996",@"Jocke Berg"];
    self.Answer1 = @[@"Andy Warhol",@"Pink Floyd",@"Please Please Me",@"T Hawkins",@"S Tyler and Perry",@"Gibson",@"Blue Dog Records",@"Randy",@"1991",@"Adam Gren"];
    self.Answer2 = @[@"Jimi Hendrix",@"ACDC",@"Rubber Soul",@"Pat Smear",@"Mick and Nikki",@"Fender",@"Blue Cat Records",@"Michael",@"1999",@"Carl Johnson"];
    self.Answer3 = @[@" Bob Dylan",@"Whitney Houston",@" Help",@"Krist Novoselic",@"A Rose and Slash",@"Ibanez",@"Eagle Records",@"Tito",@"1993",@"Jocke Berg"];
    self.Answer4 = @[@"Janis Joplin",@"Meat Loaf",@"With The Beatles",@"Dave Grohl",@"Page and Plant",@"Jackson",@"Apple",@"Marlon",@"1996",@"Stefan Larsson"];
    }


-(NSString*)setQuestion {
    NSString *question = [self.questions objectAtIndex:self.Idtest];
    return  question;
    
}
-(NSString*)setRightAnswer {
   NSString *rightanswer = [self.rightAnswers objectAtIndex:self.Idtest];
       return  rightanswer;
    
}
-(NSString*)setAnswer1 {
    NSString *answer1 = [self.Answer1 objectAtIndex:self.Idtest];
    return  answer1;
    
}

-(NSString*)setAnswer2 {
    NSString *answer2 = [self.Answer2 objectAtIndex:self.Idtest];
    return  answer2;
    
}

-(NSString*)setAnswer3 {
    NSString *answer3 = [self.Answer3 objectAtIndex:self.Idtest];
    return  answer3;
    
}
-(NSString*)setAnswer4 {
    NSString *answer4 = [self.Answer4 objectAtIndex:self.Idtest];
    return  answer4;
    
}


-(BOOL)getResult: (NSString*)answer{
    for(int i = 0; i < self.rightAnswers.count; i++){
    if(answer == [self.rightAnswers objectAtIndex:i]){
        return YES;
    }
    }
    return NO;
}
@end

//
//  ViewController.m
//  Quiz
//
//  Created by Daniel Ungerfält on 03/02/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import "ViewController.h"
#import "Game.h"



@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *questiontext;
@property (nonatomic) Game *game;
@property (weak, nonatomic) IBOutlet UIButton *buttontext;
@property (weak, nonatomic) IBOutlet UILabel *resulttext;
@property (weak, nonatomic) IBOutlet UIButton *buttontext2;
@property (weak, nonatomic) IBOutlet UIButton *buttontext3;
@property (weak, nonatomic) IBOutlet UIButton *buttontext4;

@end

@implementation ViewController

-(Game*)game{
    if(!_game){
        _game = [[Game alloc] init];
    }
    return _game;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self newQuestion];
}



- (IBAction)nextQuestion:(id)sender {
    [self.game generateNewQuestion];
    [self newQuestion];
    self.resulttext.text = @"";
    
   }

-(void)newQuestion{
    self.questiontext.text = [self.game setQuestion ];
    
    [self.buttontext setTitle: self.game.setAnswer1 forState: UIControlStateNormal];
    [self.buttontext2 setTitle: self.game.setAnswer2 forState: UIControlStateNormal];
    [self.buttontext3 setTitle: self.game.setAnswer3 forState: UIControlStateNormal];
    [self.buttontext4 setTitle: self.game.setAnswer4 forState: UIControlStateNormal];
}

- (IBAction)answer:(id)sender {
    //[self.game //getResult:self.buttontext2.titleLabel.text];
    if(self.buttontext.isTouchInside && [self.game getResult:self.buttontext.titleLabel.text]){
        self.resulttext.text = @"CORRECT!";
    } else if(self.buttontext2.isTouchInside && [self.game getResult:self.buttontext2.titleLabel.text]) {
        self.resulttext.text = @"CORRECT!";
    }else if(self.buttontext3.isTouchInside && [self.game getResult:self.buttontext3.titleLabel.text]) {
        self.resulttext.text = @"CORRECT!";
    }else if(self.buttontext4.isTouchInside && [self.game getResult:self.buttontext4.titleLabel.text]) {
        self.resulttext.text = @"CORRECT!";
    }else {
         self.resulttext.text = @"FALSE!";
    }
}
@end

//
//  Game.h
//  Quiz
//
//  Created by Daniel Ungerfält on 03/02/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Game : NSObject

-(NSString*)setQuestion;
-(NSString*)setRightAnswer;
-(NSString*)setAnswer1;
-(NSString*)setAnswer2;
-(NSString*)setAnswer3;
-(NSString*)setAnswer4;
-(void)generateNewQuestion;
-(BOOL)getResult:(NSString*)answer;


@end

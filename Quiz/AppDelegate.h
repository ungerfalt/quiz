//
//  AppDelegate.h
//  Quiz
//
//  Created by Daniel Ungerfält on 03/02/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

